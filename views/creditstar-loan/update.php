<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarLoan */

$this->title = 'Update Creditstar Loan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Creditstar Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="creditstar-loan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
