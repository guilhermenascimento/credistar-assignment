<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarLoan */

$this->title = 'Create Creditstar Loan';
$this->params['breadcrumbs'][] = ['label' => 'Creditstar Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditstar-loan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
