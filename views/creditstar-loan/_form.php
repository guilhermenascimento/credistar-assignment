<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\date\DatePicker;
use \kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarLoan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditstar-loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'user_id')->widget(Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(
                \app\models\CreditstarUser::find()->asArray()->all(),
                'id',
                function ($model, $defaultValue) {
                    return $model['first_name'] . ' ' . $model['last_name'];
                }
            ),
            'options' => ['placeholder' => 'Select a User ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'amount')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'interest')->textInput() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'duration')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
            'options' => ['placeholder' => 'Enter birth date ...'],
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'dd-M-yyyy'
            ]
        ]); ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'end_date')->widget(DatePicker::class, [
            'options' => ['placeholder' => 'Enter birth date ...'],
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]); ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'campaign')->textInput() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'status')->dropDownList(
            [
                1 => 'Active',
                0 => 'Inactive'
            ]
        ) ?>
    </div>
    <div class="clearfix"></div>
    <div class="form-group text-right">
        <?= Html::submitButton('Save', ['class' => 'credistar-btn']) ?>
        <?= Html::resetButton('Reset', ['class' => 'credistar-btn credistar-btn-red']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
