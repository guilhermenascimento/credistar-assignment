<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarLoan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Creditstar Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="creditstar-loan-view">

    <h1>Loan #<?= Html::encode($model->id) ?> - <?= Html::encode($model->user->first_name . ' ' . $model->user->last_name) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amount',
            'interest',
            'duration',
            'start_date',
            'end_date',
            'campaign',
            'status:boolean',
        ],
    ]) ?>

    <?php if(!\Yii::$app->user->getIsGuest()) { ?>
        <div class="text-right">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'credistar-btn']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'credistar-btn credistar-btn-red',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
    <?php } ?>
</div>
