<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarLoanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditstar-loan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'user') ?>
    </div>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'amount') ?>
    </div>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'interest') ?>
    </div>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'duration') ?>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Search', ['class' => 'credistar-btn']) ?>
        <?= Html::resetButton('Reset', ['class' => 'credistar-btn credistar-btn-red']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
