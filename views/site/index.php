<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to the Credistar Recruiting Assignment App!</h1>

        <p class="lead">*Note: All data manipulation features are only available upon log in.</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-6 text-center">
                <h2>Users*</h2>

                <p>Access all user management features</p>

                <p><a class="credistar-btn" href="index.php/creditstar-user">User</a></p>
            </div>
            <div class="col-md-6 text-center">
                <h2>Loans*</h2>

                <p>Access all user management features</p>

                <p><a class="credistar-btn" href="index.php/creditstar-loan">Loans</a></p>
            </div>
        </div>

    </div>
</div>
