<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarUser */

$this->title = 'Create Creditstar User';
$this->params['breadcrumbs'][] = ['label' => 'Creditstar Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditstar-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
