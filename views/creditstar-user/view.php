<?php

use app\models\CreditstarUser;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarUser */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Creditstar Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="creditstar-user-view">

    <h1>#<?= Html::encode($model->id) ?> - <?= Html::encode($model->first_name . ' ' . $model->last_name) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'email:ntext',
            'personal_code',
            'phone',
            'active:boolean',
            'dead:boolean',
            'lang:ntext',
            [
                'attribute' => 'age',
                'value' => CreditstarUser::getAgeFromCode($model->personal_code)
            ],
        ],
    ]) ?>

    <?php if(!\Yii::$app->user->getIsGuest()) { ?>
    <div class="text-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'credistar-btn']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'credistar-btn credistar-btn-red',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <?php } ?>


</div>
