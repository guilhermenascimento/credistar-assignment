<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarUserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditstar-user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'first_name') ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'last_name') ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'email') ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'personal_code') ?>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Search', ['class' => 'credistar-btn']) ?>
        <?= Html::resetButton('Reset', ['class' => 'credistar-btn credistar-btn-red']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
