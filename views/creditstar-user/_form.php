<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditstarUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditstar-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'first_name')->textInput() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'last_name')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'personal_code')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'phone')->textInput() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'lang')->textInput() ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'active')->checkbox() ?>
    </div>
    <div class="col-sm-12 col-md-6">
        <?= $form->field($model, 'dead')->checkbox() ?>
    </div>
    <div class="clearfix"></div>

    <div class="form-group text-right">
        <?= Html::submitButton('Save', ['class' => 'credistar-btn']) ?>
        <?= Html::resetButton('Reset', ['class' => 'credistar-btn credistar-btn-red']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
