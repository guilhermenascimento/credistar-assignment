<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditstarUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Creditstar Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditstar-user-index">

    <div class="col-sm-12 col-md-9">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-sm-12 col-md-3 text-right">
        <span>
            <?= Html::a('Create Creditstar User', ['create'], ['class' => 'credistar-btn']) ?>
        </span>
    </div>

    <div class="clearfix"></div>
    <?= \yii\bootstrap\Collapse::widget([
        'items' => [
            'Search' => [
                'content' => $this->render('_search', ['model' => $searchModel]),
            ],
        ]
    ]);
    ?>


    <div class="divider">&nbsp;</div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' =>
            ['class' => 'table action-table']
        ,
        'columns' => [
            'id',
            'first_name:ntext',
            'last_name:ntext',
            'email:ntext',
            'personal_code',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> ',
                            $url,
                            ['class' => 'credistar-btn credistar-btn-blue action-icons']);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> ',
                            $url,
                            ['class' => 'credistar-btn action-icons']);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> ',
                            $url,
                            [
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this user?'),
                                'data-method' => 'post',
                                'class' => 'credistar-btn credistar-btn-red action-icons'
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'update' => function ($model) {
                        return !\Yii::$app->user->getIsGuest();
                    },
                    'delete' => function ($model) {
                        return !\Yii::$app->user->getIsGuest();
                    },
                ],
            ],
        ],
    ]); ?>


</div>
