<?php

use yii\db\Migration;

/**
 * Class m200223_234425_add_foreign_key
 */
class m200223_234425_add_foreign_key extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey('FK_loan_user', 'loan', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_loan_user', 'loan');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200223_234425_add_foreign_key cannot be reverted.\n";

        return false;
    }
    */
}
