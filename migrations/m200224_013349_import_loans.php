<?php

use \app\models\CreditstarLoan;
use yii\db\Migration;

/**
 * Class m200224_013349_import_loans
 */
class m200224_013349_import_loans extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ($this->loadDataFile() as $data) {
            $model = new CreditstarLoan();
            $model->id = $data['id'];
            $model->user_id = $data['user_id'];
            $model->amount = $data['amount'];
            $model->interest = $data['interest'];
            $model->duration = $data['duration'];
            $model->start_date = date('Y-m-d', $data['start_date']);
            $model->end_date = date('Y-m-d', $data['end_date']);
            $model->campaign = $data['campaign'];
            $model->status = $data['status'];

            if($model->validate()) {
                $loan = CreditstarLoan::findOne($data['id']);
                if(!$loan) {
                    $model->save();
                }
            } else {
                echo "Loan #" . $data['id'] . " - skipped\n";
            }
            $model->save();
        }
    }

    public function loadDataFile () : array {
        $jsonFile = file_get_contents( __DIR__ . '/../loans.json');
        return \yii\helpers\Json::decode($jsonFile);;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        foreach ($this->loadDataFile() as $data) {
            $model = CreditstarLoan::findOne(intval($data['id']));
            if($model) {
                $model->delete();
                echo "Loan #" . $data['id'] . " deleted\n";
            }
        }
        return true;
    }

}
