<?php

use app\models\CreditstarUser;
use yii\db\Migration;

/**
 * Class m200224_013339_import_users
 */
class m200224_013339_import_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ($this->loadDataFile() as $data) {
            $model = new CreditstarUser();

            $model->id = $data['id'];
            $model->first_name = $data['first_name'];
            $model->last_name = $data['last_name'];
            $model->email = $data['email'];
            $model->personal_code = $data['personal_code'];
            $model->phone = $data['phone'];
            $model->active = $data['active'];
            $model->dead = $data['dead'];
            $model->lang = $data['lang'];

            if($model->validate()) {
                $user = CreditstarUser::findOne($data['id']);
                if(!$user) {
                    $model->save();
                }
            } else {
                echo "User #" . $data['id'] . " - skipped\n";
            }
        }
    }

    public function loadDataFile () : array {
        $jsonFile = file_get_contents( __DIR__ . '/../users.json');
        return \yii\helpers\Json::decode($jsonFile);;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        foreach ($this->loadDataFile() as $data) {
            $model = CreditstarUser::findOne($data['id']);
            if($model) {
                $model->delete();
                echo "User #" . $data['id'] . " deleted\n";
            }
        }
        return true;
    }

}
