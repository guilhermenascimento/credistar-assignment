<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool|null $active
 * @property bool|null $dead
 * @property string|null $lang
 */
class CreditstarUser extends \yii\db\ActiveRecord
{
    const PERSONAL_CODE_REGEX = '/^[1-6][0-9]{2}[0-1][0-9][0-9]{2}[0-9]{4}$/';

    public static $majorityAge = 18;

    /**
     * @return int
     */
    public function getMajorityAge(): int
    {
        return $this->majorityAge;
    }

    /**
     * @param int $majorityAge
     */
    public function setMajorityAge(int $majorityAge): void
    {
        $this->majorityAge = $majorityAge;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['email'], 'email'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
//            For Testing propose I am not validating the personal code so we have enough data.
            [['personal_code'], 'validatePersonalCode'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    /**
     * Validate person code
     *
     * Function extended and adapted from {https://github.com/renekorss/personal-id-code-php}
     * @return boolean True if valid personal code, false otherwise
     */
    public function validatePersonalCode($attribute, $params) : bool
    {
        $error = [];
        $code = @$this->$attribute ?: 0;
        // Personal code is 11 digits long
        if (strlen($code) !== 11) {
            $error = [
                'flag'=> true,
                'Message' => 'Longer than 11',
            ];
        }

        // Validate against regex
        if (!preg_match(self::PERSONAL_CODE_REGEX, $code)) {
            $error = [
                'flag'=> true,
                'Message' => 'Invalid',
            ];
        }

        // Check control number
        $controlNumber = (int)substr($code, -1);
        if ($controlNumber !== $this->getControlNumber($code)) {
            $error = [
                'flag'=> true,
                'Message' => 'Wrong Control Number',
            ];
        }

        if($error){
            $this->addError($attribute, Yii::t('yii', 'You entered an invalid personal code.'));
            return false;
        }

        return true;
    }

    /**
     * Get personal code control number
     *
     * Function extended and adapted from {https://github.com/renekorss/personal-id-code-php}
     *
     */
    private function getControlNumber($code) :int
    {
        $total = 0;
        for ($i = 0; $i < 10; $i++) {
            $multiplier = $i + 1;
            $total += intval(substr($code, $i, 1)) * ($multiplier > 9 ? 1 : $multiplier);
        }

        $modulo = $total % 11;

        // Second round
        if ($modulo === 10) {
            for ($i = 0; $i < 10; $i++) {
                $multiplier = $i + 3;
                $total += substr($code, $i, 1) * ($multiplier > 9 ? $multiplier - 9 : $multiplier);
            }

            $modulo = $total % 11;
            if ($modulo === 10) {
                $modulo = 0;
            }
        }

        return $modulo;
    }

    public function canUserApplyForLoan() : bool
    {
        $age = self::getAgeFromCode($this->personal_code);
        if($age < self::$majorityAge) {
            return false;
        }
        return true;
    }

    /**
     * Get user age from a given personal code
     *
     * @return int User age
     */
    public static function getAgeFromCode($code): int
    {
        $birthDate = self::getBirthDateFromCode($code);
        $date = new \DateTime();

        return $date->diff($birthDate)->y;
    }

    /**
     * Get person birthday from a given personal code
     *
     * @return \Datetime User birthday
     */
    public static function getBirthDateFromCode($code): \DateTime
    {
        $gc = substr($code, 0, 1);
        $birthDate = substr($code, 1, 6);
        switch ($gc) {
            case 1:
            case 2:
                $birthDate = '18' . $birthDate;
                break;
            case 3:
            case 4:
                $birthDate = '19' . $birthDate;
                break;
            case 5:
            case 6:
                $birthDate = '20' . $birthDate;
                break;

            default :
                return false;
                break;
        }

        $date = new \DateTime($birthDate);
        return $date;
    }
}
