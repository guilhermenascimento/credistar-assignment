<?php
namespace tests\models;
use app\models\CreditstarUser;

class CreditstarUserTest extends \Codeception\Test\Unit
{

    public function testCanUserApplyForLoan()
    {
        $user = CreditstarUser::findOne(7520);
        expect_that($user->canUserApplyForLoan());
        expect_not(false);
    }
}
